# Terraform AWS S3 bucket Module

This Terraform module provisions an Amazon S3 bucket with customizable configurations based on your environment. This deployment has the goal to demonstrate a simple Terraform pipeline execution.

## Usage

### Module Invocation

Invoke the S3 Bucket module with the desired configurations.

```hcl
module "s3-bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.15.1"

  bucket_prefix = "${var.bucket[terraform.workspace]}-${terraform.workspace}-"
}
```

## Requirements

- Terraform Version: This module is tested with Terraform version 0.14 and above.
- AWS Credentials: Ensure valid AWS credentials are configured for Terraform to authenticate with AWS.

## Providers

Ensure that the AWS provider is configured in your main Terraform configuration file (main.tf).

```hcl
provider "aws" {
  region = var.aws_region[terraform.workspace]

}
```

| Name | Version |
|------|---------|
| aws | ~> 5.29 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| s3-bucket | terraform-aws-modules/s3-bucket/aws | 3.15.1 |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aws_region | AWS region that the resources will be created | `map(string)` | ```{ "dev": "eu-central-1", "prod": "eu-west-1" }``` | no |
| bucket | Bucket name | `map(string)` | ```{ "dev": "workshop-foo", "prod": "workshop-bar" }``` | no |

## Outputs

| Name | Description |
|------|-------------|
| bucket_name | Bucket name |
| environment | Terraform environment |

## License

This module is open source and available under the MIT License. Feel free to customize and extend it according to your project requirements.

## Authors

Maintained by João Claudio.

For contributions, bug reports, or issues, please visit the GitHub repository.
