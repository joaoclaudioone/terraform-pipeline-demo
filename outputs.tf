output "bucket_name" {
  description = "Bucket name"
  value       = module.s3-bucket.s3_bucket_id
}

output "environment" {
  description = "Terraform environment"
  value       = terraform.workspace
}