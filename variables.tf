variable "aws_region" {
  description = "AWS region that the resources will be created"
  type        = map(string)
  default = {
    dev  = "eu-central-1"
    prod = "eu-west-1"
  }
}

variable "bucket" {
  description = "Bucket name"
  type        = map(string)
  default = {
    dev  = "workshop-foo"
    prod = "workshop-bar"
  }
}