provider "aws" {
  region = var.aws_region[terraform.workspace]
}

module "s3-bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.15.1"

  bucket_prefix = "${var.bucket[terraform.workspace]}-${terraform.workspace}-"
}
