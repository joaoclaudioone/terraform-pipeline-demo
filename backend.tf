terraform {
  required_providers {
    aws = "~> 5.29"
  }
  backend "s3" {
    key                  = "terraform/workshop/terraform.state"
    bucket               = "workshop-state-file-dez2023"
    workspace_key_prefix = "terraform-states"
    region               = "eu-west-1"
    encrypt              = true
    acl                  = "bucket-owner-full-control"
  }
}
